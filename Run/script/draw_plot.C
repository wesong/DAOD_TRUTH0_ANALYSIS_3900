void draw_plot(){
	float Mass[5]={0.548,105.658,139.570,493.677,938.27};
	TFile* f1=new TFile("/afs/cern.ch/work/w/wesong/private/pipi_jpsi/MC_signal_check_analysis/MyTruthAnalysis/Run/Zcm6/submitDir/data-myTruthOutput/zcm6.root");
	TTree* t1=(TTree*)f1->Get("truthTree");
	TH1F* h_Jpsimass=    new TH1F("h_Jpsimass","",    100,2700,3600);
	TH1F* h_Y4260mass=   new TH1F("h_Y4260mass","",100,3900,9000);
	TH1F* h_Z3900mass=   new TH1F("h_Z3900mass","",  100,3600,6000);
	TH1F* h_Z3900pmass=  new TH1F("h_Z3900pmass","",  100,3600,6000);
	TH1F* h_Z3900mmass=  new TH1F("h_Z3900mmass","",  100,3600,6000);
	TH1F* h_Bpmass=      new TH1F("h_Bpmass","",  100,4800,7000);
	std::vector<float> *Mup_pt=0;
	std::vector<float> *Mum_pt=0;
	std::vector<float> *Pip_pt=0;
	std::vector<float> *Pim_pt=0;
	std::vector<float> *Kp_pt=0;
	std::vector<float> *Mup_eta=0;
	std::vector<float> *Mup_phi=0;
	std::vector<float> *Mum_eta=0;
	std::vector<float> *Mum_phi=0;
	std::vector<float> *Pip_eta=0;
	std::vector<float> *Pip_phi=0;
	std::vector<float> *Pim_eta=0;
	std::vector<float> *Pim_phi=0;
	std::vector<float> *Kp_eta=0;
	std::vector<float> *Kp_phi=0;
	std::vector<float> *Z3900_charge=0;
	t1->SetBranchAddress("Mup_eta",&Mup_eta);
	t1->SetBranchAddress("Mup_pt", &Mup_pt);
	t1->SetBranchAddress("Mup_phi",&Mup_phi);
	t1->SetBranchAddress("Mum_eta",&Mum_eta);
	t1->SetBranchAddress("Mum_pt", &Mum_pt);
	t1->SetBranchAddress("Mum_phi",&Mum_phi);
	t1->SetBranchAddress("Pip_eta",&Pip_eta);
	t1->SetBranchAddress("Pip_pt", &Pip_pt);
	t1->SetBranchAddress("Pip_phi",&Pip_phi);
	t1->SetBranchAddress("Pim_eta",&Pim_eta);
	t1->SetBranchAddress("Pim_pt", &Pim_pt);
	t1->SetBranchAddress("Pim_phi",&Pim_phi);
	t1->SetBranchAddress("Kp_eta",&Kp_eta);
	t1->SetBranchAddress("Kp_pt", &Kp_pt);
	t1->SetBranchAddress("Kp_phi",&Kp_phi);
	t1->SetBranchAddress("Z3900_charge",&Z3900_charge);
	int Nevt=t1->GetEntries();
	cout<<"Total events : "<<Nevt<<endl;
	for(int i=0;i<Nevt;i++){
		cout<<i<<endl;
		t1->GetEntry(i);
		//cout<<Z3900_charge->at(0)<<" "<<(*Z3900_charge)[0]<<" "<<Mup_eta->at(0)<<" "<<Mup_eta->at(0)<<endl;
		//cout<<Mup_pt->at(0)<<" "<<Mum_pt->at(0)<<" "<<Pip_pt->at(i)<<" "<<Pim_pt->at(i)<<endl;
		//cout<<Mup_pt->at(0)<<" "<<(*Mup_eta)[0]<<" "<<(*Mup_phi)[0]<<" "<<(*Mup_eta)[1]<<" "<<(*Mup_eta)[2]<<" "<<Z3900_charge->at(0)<<" "<<(*Z3900_charge)[0]<<endl;
		TLorentzVector p4mup,p4mum,p4pip,p4pim,p4kp;
		p4mup.SetPtEtaPhiM(Mup_pt->at(i),Mup_eta->at(i),Mup_phi->at(i),Mass[1]);                                           
		p4mum.SetPtEtaPhiM(Mum_pt->at(i),Mum_eta->at(i),Mum_phi->at(i),Mass[1]);                                           
		p4pip.SetPtEtaPhiM(Pip_pt->at(i),Pip_eta->at(i),Pip_phi->at(i),Mass[2]);                                           
		p4pim.SetPtEtaPhiM(Pim_pt->at(i),Pim_eta->at(i),Pim_phi->at(i),Mass[2]);                                           
		//p4kp.SetPtEtaPhiM(Kp_pt->at(0),Kp_eta->at(0),Kp_phi->at(0),Mass[3]);                                           
		h_Jpsimass->Fill((p4mup+p4mum).M());
		h_Y4260mass->Fill((p4pip+p4pim+p4mup+p4mum).M());
		h_Bpmass->Fill((p4kp+p4pip+p4pim+p4mup+p4mum).M());
		if((*Z3900_charge)[0]>0.){
			h_Z3900mass->Fill((p4pip+p4mup+p4mum).M());
			h_Z3900pmass->Fill((p4pip+p4mup+p4mum).M());
		}
		else {
			h_Z3900mass->Fill((p4pim+p4mup+p4mum).M());
			h_Z3900mmass->Fill((p4pim+p4mup+p4mum).M());
		}
		//cout<<(p4mup+p4mum).M()<<" "<<(p4pip+p4mup+p4mum).M()<<" "<<(p4pip+p4pim+p4mup+p4mum).M()<<" "<<(p4kp+p4pip+p4pim+p4mup+p4mum).M()<<" "<<Pip_pt->at(0)<<" "<<Pim_pt->at(0)<<endl;
	}
	TCanvas* c_Jpsi=new TCanvas("c_Jpsi","mass of Jpsi");
	h_Jpsimass->Draw("E");
	TCanvas* c_Y4260=new TCanvas("c_Y4260","mass of Y4260");
	h_Y4260mass->Draw("E");
	TCanvas* c_Z3900=new TCanvas("c_Z3900","mass of Z3900");
	h_Z3900mass->Draw("E");
	TCanvas* c_Z3900p=new TCanvas("c_Z3900p","mass of Z3900+");
	h_Z3900pmass->Draw("E");
	TCanvas* c_Z3900m=new TCanvas("c_Z3900m","mass of Z3900-");
	h_Z3900mmass->Draw("E");
	TCanvas* c_Bp=new TCanvas("c_Bp","mass of Bp");
	h_Bpmass->Draw("E");
}
