#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyTruthAnalysis/TruthAnalysis.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "xAODRootAccess/tools/Message.h"

#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

#include <TFile.h>

// EDM includes:
#include "xAODEventInfo/EventInfo.h"

#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
	do {                                                     \
		if( ! EXP.isSuccess() ) {                             \
			Error( CONTEXT,                                    \
					XAOD_MESSAGE( "Failed to execute: %s" ),    \
#EXP );                                     \
			return EL::StatusCode::FAILURE;                    \
		}                                                     \
	} while( false )


// this is needed to distribute the algorithm to the workers
ClassImp(TruthAnalysis)



TruthAnalysis :: TruthAnalysis ()
{
	// Here you put any code for the base initialization of variables,
	// e.g. initialize all pointers to 0.  Note that you should only put
	// the most basic initialization here, since this method will be
	// called on both the submission and the worker node.  Most of your
	// initialization code will go into histInitialize() and
	// initialize().
}



EL::StatusCode TruthAnalysis :: setupJob (EL::Job& job)
{
	// Here you put code that sets up the job on the submission object
	// so that it is ready to work with your algorithm, e.g. you can
	// request the D3PDReader service or add output files.  Any code you
	// put here could instead also go into the submission script.  The
	// sole advantage of putting it here is that it gets automatically
	// activated/deactivated when you add/remove the algorithm from your
	// job, which may or may not be of value to you.

	// let's initialize the algorithm to use the xAODRootAccess package
	job.useXAOD ();
	EL_RETURN_CHECK( "setupJob(), calling xAOD::Init()", xAOD::Init() ); // call before opening first file


	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: histInitialize ()
{
	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.

	h_muPt = new TH1F("h_muPt", "Muon p_{T};p_{T}(#mu) [GeV];Arbitrary Units", 100, 0., 50.); // jet pt [GeV]
	wk()->addOutput (h_muPt);

	// get the output file, create a new TTree and connect it to that output
	// define what braches will go in that tree
	TFile *outputFile = wk()->getOutputFile(outputName);

	truthTree = new TTree ("truthTree", "truthTree");
	truthTree->SetDirectory(outputFile);
	truthTree->Branch("EventNumber", &EventNumber);
	truthTree->Branch("Bp_pt", &Bp_pt);
	truthTree->Branch("Bp_eta", &Bp_eta);
	truthTree->Branch("Bp_phi", &Bp_phi);
	truthTree->Branch("Jpsi_pt", &Jpsi_pt);
	truthTree->Branch("Jpsi_eta", &Jpsi_eta);
	truthTree->Branch("Jpsi_phi", &Jpsi_phi);
	truthTree->Branch("Y4260_pt",  &Y4260_pt);
	truthTree->Branch("Y4260_eta", &Y4260_eta);
	truthTree->Branch("Y4260_phi", &Y4260_phi);
	truthTree->Branch("Z3900_pt",  &Z3900_pt);
	truthTree->Branch("Z3900_eta", &Z3900_eta);
	truthTree->Branch("Z3900_phi", &Z3900_phi);
	truthTree->Branch("Z3900_charge",&Z3900_charge);
	truthTree->Branch("B_charge",&B_charge);
	truthTree->Branch("Mup_pt", &Mup_pt);
	truthTree->Branch("Mup_eta", &Mup_eta);
	truthTree->Branch("Mup_phi", &Mup_phi);
	truthTree->Branch("Mum_pt", &Mum_pt);
	truthTree->Branch("Mum_eta", &Mum_eta);
	truthTree->Branch("Mum_phi", &Mum_phi);
	truthTree->Branch("Kp_pt", &Kp_pt);
	truthTree->Branch("Kp_eta", &Kp_eta);
	truthTree->Branch("Kp_phi", &Kp_phi);
	truthTree->Branch("Pip_pt",  &Pip_pt);
	truthTree->Branch("Pip_eta", &Pip_eta);
	truthTree->Branch("Pip_phi", &Pip_phi);
	truthTree->Branch("Pim_pt",  &Pim_pt);
	truthTree->Branch("Pim_eta", &Pim_eta);
	truthTree->Branch("Pim_phi", &Pim_phi);
	truthTree->Branch("nGamma_Bp", &nGamma_Bp);
	truthTree->Branch("nGamma_Jpsi", &nGamma_Jpsi);
	truthTree->Branch("nGamma_Y4260", &nGamma_Y4260);
	truthTree->Branch("nGamma_Z3900", &nGamma_Z3900);

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: fileExecute ()
{
	// Here you do everything that needs to be done exactly once for every
	// single file, e.g. collect a list of all lumi-blocks processed
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: changeInput (bool firstFile)
{
	// Here you do everything you need to do when we change input files,
	// e.g. resetting branch addresses on trees.  If you are using
	// D3PDReader or a similar service this method is not needed.
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: initialize ()
{
	// Here you do everything that you need to do after the first input
	// file has been connected and before the first event is processed,
	// e.g. create additional histograms based on which variables are
	// available in the input files.  You can also create all of your
	// histograms and trees in here, but be aware that this method
	// doesn't get called if no events are processed.  So any objects
	// you create here won't be available in the output if you have no
	// input events.

	xAOD::TEvent* event = wk()->xaodEvent();

	// as a check, let's see the number of events in our xAOD
	Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

	m_truthEventsKey    = "TruthEvents";
	m_truthParticlesKey = "TruthParticles";
	m_truthVerticesKey  = "TruthVertices";

	m_eventCounter = 0;

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: execute ()
{
	// Here you do everything that needs to be done on every single
	// events, e.g. read input variables, apply cuts, and fill
	// histograms and trees.  This is where most of your actual analysis
	// code will go.

	xAOD::TEvent* event = wk()->xaodEvent();

	// print every 100 events, so we know where we are:
	if( (m_eventCounter % 100) == 0 ) 
		Info("execute()", "Event number = %i", m_eventCounter );

	m_eventCounter++;

	//----------------------------
	// Event information
	//--------------------------- 
	const xAOD::EventInfo* eventInfo = 0;
	EL_RETURN_CHECK("execute(), retrieving EventInfo",event->retrieve( eventInfo, "EventInfo"));

	clearBranches();

	EventNumber = eventInfo->eventNumber();

	// check if the event is data or MC
	// (many tools are applied either to data or MC)
	bool isMC = false;
	// check if the event is MC
	if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
		isMC = true; // can do something with this later

	if( !isMC ) {
		Info("execute()", "Not MC event!!!");
		return EL::StatusCode::SUCCESS;
	}

	const xAOD::TruthEventContainer   * truthEvents(nullptr);
	const xAOD::TruthParticleContainer* truthParticles(nullptr);
	const xAOD::TruthVertexContainer  * truthVertices(nullptr);
	EL_RETURN_CHECK("execute(), retrieving TruthEvents", event->retrieve( truthEvents,    m_truthEventsKey));
	EL_RETURN_CHECK("execute(), retrieving TruthParticles", event->retrieve( truthParticles, m_truthParticlesKey));
	EL_RETURN_CHECK("execute(), retrieving TruthVertices", event->retrieve( truthVertices,  m_truthVerticesKey));


	// use jus the main interaction
	const xAOD::TruthEvent* hardTE = truthEvents->front();

	for(auto tpLink : hardTE->truthParticleLinks()) {
		if( !tpLink.isValid() ) {
			Info("execute()", "Invalid particleLink!!!");
			continue;
		}
		const xAOD::TruthParticle* p = *tpLink;

		//if(abs(p->pdgId()) == 521) { // B+
		if((p->pdgId()) == -521) { // B+
			//Info("execute()", "  B+ with barcode = %i, pT = %f", p->barcode(), p->pt());
			Bp_pt. push_back(p->pt());
			Bp_eta.push_back(p->eta());
			Bp_phi.push_back(p->phi());
			if(p->pdgId()==521) B_charge.push_back(1);
			else B_charge.push_back(-1);
			int nGammasBp(0);

			// look at mother first
			if(p->hasProdVtx()) {
				assert(p->prodVtx());
				for(auto bpMotherLink : p->prodVtx()->incomingParticleLinks() ) {
					if( !bpMotherLink.isValid() ) {
						Info("execute()", "Invalid B+ motherParticleLink!!!");
						continue;
					}
					auto mother = *bpMotherLink;
					//Info("execute()", "    B+ mother pdg = %i, barcode = %i, pT = %f", mother->pdgId(), mother->barcode(), mother->pt());
				}
			}
			else {
				Info("execute()", "B+ doesn't have prodVtx!!!");
			}

			// now Y4260 decay products
			if(p->hasDecayVtx()) {
				assert(p->decayVtx());
				for(auto bpChildLink : p->decayVtx()->outgoingParticleLinks() ) {
					if( !bpChildLink.isValid() ) {
						Info("execute()", "Invalid B+ childParticleLink!!!");
						continue;
					}
					auto child = *bpChildLink;

					if(child->status() == 3) continue; // Photos's history entries

					if(fabs(child->pdgId()) == 453124) { // Y4260
						Y4260_pt. push_back(child->pt());
						Y4260_eta.push_back(child->eta());
						Y4260_phi.push_back(child->phi());
						int nGammasY4260(0);
						if(child->hasDecayVtx()) {
							assert(child->decayVtx());
							for(auto y4260ChildLink : child->decayVtx()->outgoingParticleLinks() ) {
								auto ychild = *y4260ChildLink;

								if(ychild->status() == 3) continue; // Photos's history entries
								if(ychild->pdgId() == 22) { // gamma
									nGammasY4260++;
								}

								if(abs(ychild->pdgId()) == 211) { // pi+-
									if(ychild->pdgId() == 211) {
										Pip_pt.push_back(ychild->pt());
										Pip_eta.push_back(ychild->eta());
										Pip_phi.push_back(ychild->phi());
									}
									else {
										Pim_pt.push_back(ychild->pt());
										Pim_eta.push_back(ychild->eta());
										Pim_phi.push_back(ychild->phi());
									}
								}
								if(abs(ychild->pdgId()) == 463124) { // Z3900
									std::cout<<"I am 3900	"<<ychild->pdgId()<<std::endl;
									Z3900_pt. push_back(child->pt());
									Z3900_eta.push_back(child->eta());
									Z3900_phi.push_back(child->phi());
									int nGammasZ3900(0);
									if(ychild->hasDecayVtx()) {
										assert(ychild->decayVtx());
										for(auto z3900ChildLink : ychild->decayVtx()->outgoingParticleLinks() ) {
											auto zchild = *z3900ChildLink;

											if(zchild->status() == 3) continue; // Photos's history entries
											if(zchild->pdgId() == 22) { // gamma
												nGammasZ3900++;
											}
											if(zchild->pdgId() == 211) {
												Pip_pt.push_back(zchild->pt());
												Pip_eta.push_back(zchild->eta());
												Pip_phi.push_back(zchild->phi());
												Z3900_charge.push_back(1);
											}
											if(zchild->pdgId() == -211) {
												Pim_pt.push_back(zchild->pt());
												Pim_eta.push_back(zchild->eta());
												Pim_phi.push_back(zchild->phi());
												Z3900_charge.push_back(-1);
											}
											if(zchild->pdgId() == 443) { // J/psi
												//Info("execute()", "    J/psi with barcode = %i, pT = %f", child->barcode(), child->pt());
												Jpsi_pt. push_back(zchild->pt());
												Jpsi_eta.push_back(zchild->eta());
												Jpsi_phi.push_back(zchild->phi());
												int nGammasJpsi(0);

												if(zchild->hasDecayVtx()) {
													assert(zchild->decayVtx());
													for(auto jpsiChildLink : zchild->decayVtx()->outgoingParticleLinks() ) {
														auto gchild = *jpsiChildLink;

														if(gchild->status() == 3) continue; // Photos's history entries

														if(abs(gchild->pdgId()) == 13) { // mu+-
															//Info("execute()", "      mu with pdg = %i, barcode = %i, pT = %f, status = %i", gchild->pdgId(), gchild->barcode(), gchild->pt(), gchild->status());

															// Fill one histogram
															h_muPt->Fill(gchild->pt()*0.001);

															if(gchild->pdgId() == -13) {
																Mup_pt. push_back(gchild->pt());
																Mup_eta.push_back(gchild->eta());
																Mup_phi.push_back(gchild->phi());
															}
															else {
																Mum_pt. push_back(gchild->pt());
																Mum_eta.push_back(gchild->eta());
																Mum_phi.push_back(gchild->phi());
															}
														}
														else if(gchild->pdgId() == 22) { // gamma
															nGammasJpsi++;
														}
														else {
															Info("execute()", "      Unknown J/psi child with pdg = %i!!!", gchild->pdgId());
														}
													}
													//Info("execute()", "      nPhotons from J/psi = %i", nGammasJpsi);

												}
												else {
													Info("execute()", "J/psi doesn't have decayVtx!!!");
												}
												nGamma_Jpsi.push_back(nGammasJpsi);

											}
										}
									}
									nGamma_Z3900.push_back(nGammasZ3900);
								}
							}
						}
						nGamma_Y4260.push_back(nGammasY4260);
					}
					else if(abs(child->pdgId()) == 321) { // K+
						//Info("execute()", "      K+ with pdg = %i, barcode = %i, pT = %f, status = %i", child->pdgId(), child->barcode(), child->pt(), child->status());
						Kp_pt.push_back(child->pt());
						Kp_eta.push_back(child->eta());
						Kp_phi.push_back(child->phi());
					}
					else if(child->pdgId() == 22) { // gamma
						nGammasBp++;
					}
					else {
						Info("execute()", "    Unknown B+ child with pdg = %i!!!", child->pdgId());
					}
				}
				//Info("execute()", "    nPhotons from B+ = %i", nGammasBp);

			}
			else {
				Info("execute()", "B+ doesn't have decayVtx!!!");
			}
			nGamma_Bp.push_back(nGammasBp);

		}

	}

	truthTree->Fill();

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: postExecute ()
{
	// Here you do everything that needs to be done after the main event
	// processing.  This is typically very rare, particularly in user
	// code.  It is mainly used in implementing the NTupleSvc.
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: finalize ()
{
	// This method is the mirror image of initialize(), meaning it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
	// merged.  This is different from histFinalize() in that it only
	// gets called on worker nodes that processed input events.

	xAOD::TEvent* event = wk()->xaodEvent();


	return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthAnalysis :: histFinalize ()
{
	// This method is the mirror image of histInitialize(), meaning it
	// gets called after the last event has been processed on the worker
	// node and allows you to finish up any objects you created in
	// histInitialize() before they are written to disk.  This is
	// actually fairly rare, since this happens separately for each
	// worker node.  Most of the time you want to do your
	// post-processing on the submission node after all your histogram
	// outputs have been merged.  This is different from finalize() in
	// that it gets called on all worker nodes regardless of whether
	// they processed input events.
	return EL::StatusCode::SUCCESS;
}

void TruthAnalysis :: clearBranches()
{
	Bp_pt.clear();
	Bp_eta.clear();
	Bp_phi.clear();
	Jpsi_pt.clear();
	Jpsi_eta.clear();
	Jpsi_phi.clear();
	Y4260_pt.clear();
	Y4260_eta.clear();
	Y4260_phi.clear();
	Z3900_pt.clear();
	Z3900_eta.clear();
	Z3900_phi.clear();
	Z3900_charge.clear();
	B_charge.clear();
	Mup_pt.clear();
	Mup_eta.clear();
	Mup_phi.clear();
	Mum_pt.clear();
	Mum_eta.clear();
	Mum_phi.clear();
	Pip_pt.clear();
	Pip_eta.clear();
	Pip_phi.clear();
	Pim_pt.clear();
	Pim_eta.clear();
	Pim_phi.clear();
	Kp_pt.clear();
	Kp_eta.clear();
	Kp_phi.clear();
	nGamma_Bp.clear();
	nGamma_Jpsi.clear();
	nGamma_Y4260.clear();
	nGamma_Z3900.clear();
}
