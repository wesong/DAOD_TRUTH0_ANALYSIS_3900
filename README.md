# DAOD_TRUTH0_ANALYSIS_3900

setup the RootCore:

setupATLAS
rcSetup Base,2.4.27     # (next time run just rcSetup, no need to specify the release)

Compile the package:

rc find_packages
rc compile

Then run the code. A small test DAOD_TRUTH0 file with 10 signal events is placed in MyTruthAnalysis/Run/DAOD_TRUTH0.Bplus.root.

cd MyTruthAnalysis/Run
root -b -q -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'

Path to input DAOD_TRUTH0 file(s) is specified in MyTruthAnalysis/Run/ATestRun.cxx.

This will produce a directory submitDir with a few files. Among them, hist-Run.root is the file containing histograms filled in the code, data-myTruthOutput/Run.root contains the tree with the signal particle properties.

The analysis code itself is in MyTruthAnalysis/Root/TruthAnalysis.cxx and MyTruthAnalysis/MyTruthAnalysis/TruthAnalysis.h. 


Do not use it now, it is broken
