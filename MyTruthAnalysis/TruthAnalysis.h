#ifndef MyTruthAnalysis_TruthAnalysis_H
#define MyTruthAnalysis_TruthAnalysis_H

#include <TH1.h>
#include <TTree.h>

#include <EventLoop/Algorithm.h>

class TruthAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  TH1F *h_muPt; //!
  
  std::string outputName;
  
  TTree *truthTree; //!
  int EventNumber; //!
  std::vector<float> Bp_pt; //!
  std::vector<float> Bp_eta; //!
  std::vector<float> Bp_phi; //!
  std::vector<float> Jpsi_pt; //!
  std::vector<float> Jpsi_eta; //!
  std::vector<float> Jpsi_phi; //!
  std::vector<float> Y4260_pt; //!
  std::vector<float> Y4260_eta; //!
  std::vector<float> Y4260_phi; //!
  std::vector<float> Z3900_pt; //!
  std::vector<float> Z3900_eta; //!
  std::vector<float> Z3900_phi; //!
  std::vector<float> Z3900_charge; //!
  std::vector<float> B_charge; //!
  std::vector<float> Mup_pt; //!
  std::vector<float> Mup_eta; //!
  std::vector<float> Mup_phi; //!
  std::vector<float> Mum_pt; //!
  std::vector<float> Mum_eta; //!
  std::vector<float> Mum_phi; //!
  std::vector<float> Kp_pt; //!
  std::vector<float> Kp_eta; //!
  std::vector<float> Kp_phi; //!
  std::vector<float> Pip_pt; //!
  std::vector<float> Pip_eta; //!
  std::vector<float> Pip_phi; //!
  std::vector<float> Pim_pt; //!
  std::vector<float> Pim_eta; //!
  std::vector<float> Pim_phi; //!
  std::vector<float> nGamma_Bp; //!
  std::vector<float> nGamma_Jpsi; //!
  std::vector<float> nGamma_Y4260; //!
  std::vector<float> nGamma_Z3900; //!
  
  void clearBranches();


  // this is a standard constructor
  TruthAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TruthAnalysis, 1);
  
private:
  int m_eventCounter; //!
  
  std::string m_truthEventsKey,m_truthParticlesKey, m_truthVerticesKey; //!
  
};

#endif
