#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/OutputStream.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/ScanDir.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>

#include "MyTruthAnalysis/TruthAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
//   const char* inputFilePath = gSystem->ExpandPathName ("/home/turchihin/TestData/daod_truth");
//   SH::ScanDir().filePattern("DAOD_TRUTH0.my*.truth.DxAOD.root").scan(sh, inputFilePath);
  const char* inputFilePath = gSystem->ExpandPathName ("../Run"); // path should contain at least one directory, not just ./ because last dir name is used for the output filenames
  SH::ScanDir().filePattern("DAOD_TRUTH0.Bplus.root").scan(sh, inputFilePath);

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, 1000);
  
  // define an output and an ntuple associated to that output
  EL::OutputStream output("myTruthOutput");
  job.outputAdd(output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc("myTruthOutput");
  job.algsAdd(ntuple);

  // Add our analysis to the job:
  TruthAnalysis* alg = new TruthAnalysis();
  job.algsAdd( alg );
  alg->outputName = "myTruthOutput"; // give the name of the output to our algorithm

  // Run the job using the local/direct driver:
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  return 0;
} 
